package ftp

import (
    "github.com/jlaffaye/ftp"
    "io"
    "time"
)

type FtpServicesProvider interface {
    NewClient(string) error
    Login(string string) error
    Retr(path string) (io.ReadCloser, error)
    Stor(path string, r io.Reader) error
    List(path string) (filenames []string, err error)
}

type FtpService struct {
    conn *ftp.ServerConn
}

func NewFtpService() *FtpService {
    return &FtpService{}
}

func (f *FtpService) NewClient(urlAndPort string) (err error) {
    // localhost:21

    f.conn, err = ftp.DialTimeout(urlAndPort, 60*time.Second)
    return
}

func (f *FtpService) Login(user, password string) (err error) {
    err = f.conn.Login(user, password)
    return
}

func (f *FtpService) Retr(path string) (rc io.ReadCloser, err error) {
    // io.Readcloser must be closed after
    rc, err = f.conn.Retr(path)
    return
}

func (f *FtpService) Stor(path string, r io.Reader) (err error) {
    // Hint: io.Pipe() can be used if an io.Writer is required.
    err = f.conn.Stor(path, r)
    return
}

func (f *FtpService) List(path string) (filenames []string, err error) {
    // Hint: io.Pipe() can be used if an io.Writer is required.
    entities, err := f.conn.List(path)

    if len(entities) > 0 {
        for _, entity := range entities {
            if entity.Type == 0 {
                name := entity.Name
                filenames = append(filenames, name)
            }

        }

    }

    return
}